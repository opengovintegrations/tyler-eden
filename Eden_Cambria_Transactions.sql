CREATE view vw_opengov_transactions

as

SELECT

    Transaction_Amount = CASE

        WHEN aacct_type = 'R' THEN (gcredit_amount - gdebit_amount)

        WHEN genc_amount = 0 THEN (gdebit_amount - gcredit_amount)

        ELSE genc_amount

    END,

    gdoc_year                 AS FY,

    alevel_1,

    alevel_2,

    alevel_3,

    alevel_4,

    alevel_5,

    alevel_6,

    alevel_7,

    alevel_8,

    alevel_9,

    alevel_10,

    esgacttr.title_1,

    esgacttr.title_2,

    esgacttr.title_3,

    esgacttr.title_4,

    esgacttr.title_5,

    esgacttr.title_6,

    esgacttr.title_7,

    esgacttr.title_8,

    esgacttr.title_9,

    esgacttr.title_10,

    aacct_title AS ObjectName,

    xpost_date                AS Post_Date,

    xdoc_date                 AS Document_Date,

    gorig_journal             AS Original_Journal,

    gtran_type                AS Transaction_Type,

    gtran_doc_no              AS Transaction_Document_#,

    xdoc_reference            AS Document_Reference,

    gline_no                  AS Line_#,

    gsub_line_no              AS Sub_Line_#,

    gdescription              AS Description,

    aacct_no                  AS Acct_#,

    spa_string_no             AS String_#,

    gamount                   AS GL_Amount,

    gdebit_credit             AS Debit_or_Credit,

    gpa_adj_amount            AS PA_Amount,

    gdoc_year                 AS Year,

    aacct_type                AS Acct_Type,

    gdebit_amount             AS Debit_Amount,

    gcredit_amount            AS Credit_Amount,

    genc_amount               AS GL_Encumbrance_Amount,

    gbudadj_amount            AS GL_Budget_Adj_Amount,

    gpa_adj_amount            AS PA_Adjustment_Amount,

    apvend_lname              AS Vendor_Last_Name,

    apvend_fname              AS Vendor_First_Name,

    appo_num                  AS PO_#,

    apchk_no                  AS Check_#,

    apchk_date                AS Check_Date,

    apinv_date                AS Invoice_Date,

    arcust_lname              AS Customer_Last_Name,

    arcust_fname              AS Customer_First_Name,

    gacct_period              AS Acct_Period

FROM

    GoldSTD.dbo.esgtranv,

    GoldSTD.dbo.esgacttr

WHERE

    aacct_no = esgacttr.acct_no

    AND gdoc_year = esgacttr.acct_year

    AND esgacttr.title_id = -1

	AND gdoc_year >=2013
