ALTER view vw_opengov_eden_transactions_bs

as

SELECT Esxacctr.BEG_BALANCE AS Transaction_Amount,

    --Esxacctr.ACCT_YEAR                 AS FY,

    -- CUSTOM ABOVE
    Esxacctr.ACCT_ID AS aACCT_ID,
    Esxacctr.ACCT_KIND AS aACCT_KIND,
    Esxacctr.ACCT_NO AS aACCT_NO, --Acct_
    Esxacctr.ACCT_TITLE AS aACCT_TITLE, --ObjectName
    Esxacctr.ACCT_TYPE AS aACCT_TYPE, --Acct_Type
    Esxacctr.CASH_INV_CODE AS aCASH_INV_CODE,
    Esxacctr.CONTROL_TYPE AS aCONTROL_TYPE,
    REPLACE(Esxacctr.LEVEL_1, ' ','') AS aLEVEL_1,
    --Esxacctr.LEVEL_10 AS aLEVEL_10,
    REPLACE(Esxacctr.LEVEL_2, ' ','') AS aLEVEL_2,
    REPLACE(Esxacctr.LEVEL_3, ' ','') AS aLEVEL_3,
    REPLACE(Esxacctr.LEVEL_4, ' ','') AS aLEVEL_4,
    REPLACE(Esxacctr.LEVEL_5, ' ','') AS aLEVEL_5,
    --Esxacctr.LEVEL_6 AS aLEVEL_6,
    --Esxacctr.LEVEL_7 AS aLEVEL_7,
    --Esxacctr.LEVEL_8 AS aLEVEL_8,
    --Esxacctr.LEVEL_9 AS aLEVEL_9,
    Esxacctr.MGMT_INFO_CODE AS aMGMT_INFO_CODE,
    Esxacctr.NOMINAL_FLAG AS aNOMINAL_FLAG,
    '' AS apCHK_DATE, --Check_Date
    '' AS apCHK_NO, --Check_
    '' AS apINV_DATE, --Invoice_Date
    '' AS apPO_NUM, --PO_
    '' AS apPO_TYPE,
    '' AS apVEND_CODE,
    '' AS apVEND_FNAME, --Vendor_First_Name
    '' AS apVEND_LNAME, --Vendor_Last_Name
    --'' AS arCUST_FNAME, --Customer_First_Name
    --'' AS arCUST_LNAME, --Customer_Last_Name
    --'' AS arCUST_NO,
    --'' AS arDOC_NO,
    Esxacctr.REPORT_GROUP1 AS aREPORT_GROUP1,
    Esxacctr.REPORT_GROUP2 AS aREPORT_GROUP2,
    Esxacctr.STATUS_CODE AS aSTATUS_CODE,
    '' AS gACCT_PERIOD, --Acct_Period
    '' AS gAMOUNT, --GL_Amomunt
    '' AS gBUDADJ_AMOUNT, --GL_Budget_Adj_Amount
    '' AS gCREDIT_AMOUNT, --Credit_Amount
    '' AS gDEBIT_AMOUNT, --Debit_Amount
    Esxacctr.DEBIT_OR_CREDIT AS gDEBIT_CREDIT, --Debit_or_Credit
    '' AS gDESCRIPTION, --Description
    '' AS gDOC_SRC,
    '' AS gDOC_YEAR,
    '' AS gENC_AMOUNT, --GL_Encumbrance_Amount
    --'' AS gLINE_NO, --Line_
    '' AS gORIG_JOURNAL, --Original_Journal
    '' AS gPA_ADJ_AMOUNT, --PA_Amount
    --'' AS gSUB_LINE_NO, --Sub_line_
    '' AS gTRAN_DOC_NO, --Transaction_Document_
    '' AS gTRAN_TYPE, --Transaction_Type
    '' AS pALLOC_AMOUNT,
    '' AS pAMOUNT,
    '' AS pBUDADJ_AMOUNT,
    '' AS pCHARGE_TYPE,
    '' AS pDEV_DEP_CHARGE,
    '' AS pENC_AMOUNT,
    '' AS pHOURS,
    '' AS pLAB_OR_BEN,
    '' AS pPA_CLASS,
    '' AS pREC_TYPE,
    '' AS pTRAN_AMOUNT,
    '' AS sPA_STRING_NO, --String_
    '' AS sPROJ_TITLE,
    --'' AS sPROJECT_ID,
    '' AS sPROJECT_NO,
    '' AS sSTR_TITLE,
    '' AS sSTR_TITLE_1,
    '' AS sSTR_TITLE_2,
    '' AS sSTR_TITLE_3,
    '' AS sSTRING_LEVEL,
    '' AS sSTRING_TYPE,
    '' AS sSUB_TASK_NO,
    '' AS sTASK_NO,
    --'' AS sWORK_ORDER_NO,
    --'' AS xAUTO_REV,
    CONCAT('1/1/', Esxacctr.ACCT_YEAR)  AS xDOC_DATE, --Document_Date
    '' AS xDOC_DESC,
    '' AS xDOC_GROUP,
    '' AS xDOC_REFERENCE, --Document_Reference
    '' AS xPOST_DATE, --Post_Date
    '' AS xPOST_NO,
    '' AS xSUMMARY_DOC,

    esgacttr.ACCT_ID AS ACCT_ID,
    --esgacttr.ACCT_NO AS ACCT_NO,
    --esgacttr.ACCT_TITLE AS ACCT_TITLE,
    --'' AS ACCT_TYPE,
    --Esxacctr.ACCT_YEAR AS ACCT_YEAR,
    --esgacttr.LEVEL_1 AS LEVEL_1,
    --esgacttr.LEVEL_10 AS LEVEL_10,
    --esgacttr.LEVEL_11 AS LEVEL_11,
    --esgacttr.LEVEL_12 AS LEVEL_12,
    --esgacttr.LEVEL_2 AS LEVEL_2,
    --esgacttr.LEVEL_3 AS LEVEL_3,
    --esgacttr.LEVEL_4 AS LEVEL_4,
    --esgacttr.LEVEL_5 AS LEVEL_5,
    --esgacttr.LEVEL_6 AS LEVEL_6,
    --esgacttr.LEVEL_7 AS LEVEL_7,
    --esgacttr.LEVEL_8 AS LEVEL_8,
    --esgacttr.LEVEL_9 AS LEVEL_9,
    --esgacttr.SUPR_HEADER_1 AS SUPR_HEADER_1,
    --esgacttr.SUPR_HEADER_10 AS SUPR_HEADER_10,
    --esgacttr.SUPR_HEADER_11 AS SUPR_HEADER_11,
    --esgacttr.SUPR_HEADER_12 AS SUPR_HEADER_12,
    --esgacttr.SUPR_HEADER_2 AS SUPR_HEADER_2,
    --esgacttr.SUPR_HEADER_3 AS SUPR_HEADER_3,
    --esgacttr.SUPR_HEADER_4 AS SUPR_HEADER_4,
    --esgacttr.SUPR_HEADER_5 AS SUPR_HEADER_5,
    --esgacttr.SUPR_HEADER_6 AS SUPR_HEADER_6,
    --esgacttr.SUPR_HEADER_7 AS SUPR_HEADER_7,
    --esgacttr.SUPR_HEADER_8 AS SUPR_HEADER_8,
    --esgacttr.SUPR_HEADER_9 AS SUPR_HEADER_9,
    --esgacttr.SUPR_TOTAL_1 AS SUPR_TOTAL_1,
    --esgacttr.SUPR_TOTAL_10 AS SUPR_TOTAL_10,
    --esgacttr.SUPR_TOTAL_11 AS SUPR_TOTAL_11,
    --esgacttr.SUPR_TOTAL_12 AS SUPR_TOTAL_12,
    --esgacttr.SUPR_TOTAL_2 AS SUPR_TOTAL_2,
    --esgacttr.SUPR_TOTAL_3 AS SUPR_TOTAL_3,
    --esgacttr.SUPR_TOTAL_4 AS SUPR_TOTAL_4,
    --esgacttr.SUPR_TOTAL_5 AS SUPR_TOTAL_5,
    --esgacttr.SUPR_TOTAL_6 AS SUPR_TOTAL_6,
    --esgacttr.SUPR_TOTAL_7 AS SUPR_TOTAL_7,
    --esgacttr.SUPR_TOTAL_8 AS SUPR_TOTAL_8,
    --esgacttr.SUPR_TOTAL_9 AS SUPR_TOTAL_9,
    esgacttr.TITLE_1 AS TITLE_1,
    --esgacttr.TITLE_10 AS TITLE_10,
    --esgacttr.TITLE_11 AS TITLE_11,
    --esgacttr.TITLE_12 AS TITLE_12,
    esgacttr.TITLE_2 AS TITLE_2,
    esgacttr.TITLE_3 AS TITLE_3,
    esgacttr.TITLE_4 AS TITLE_4,
    esgacttr.TITLE_5 AS TITLE_5,
    --esgacttr.TITLE_6 AS TITLE_6,
    --esgacttr.TITLE_7 AS TITLE_7,
    --esgacttr.TITLE_8 AS TITLE_8,
    --esgacttr.TITLE_9 AS TITLE_9,
    esgacttr.TITLE_ID AS TITLE_ID,
    'Beginning Balances' AS OGRecordType

    FROM EDENLIVE.dbo.Esxacctr, EDENLIVE.dbo.esgacttr
    WHERE Esxacctr.ACCT_NO = esgacttr.acct_no
    AND Esxacctr.ACCT_YEAR = esgacttr.acct_year
    AND esgacttr.title_id = -1
  AND Esxacctr.ACCT_YEAR >= 2014 and Esxacctr.ACCT_TYPE = 'B'
