ALTER VIEW vw_opengov_budgets

AS

SELECT

    ESGBUDGD.ACCT_ID,

    ESXACCTR.ACCT_YEAR AS "Fiscal Year",

	ESGBUDGD.Period,

    ESGBUDGD.BUD_AMOUNT AS TotalBudgetAmount,

	'Adopted' AS BudgetType,

    ESXACCTR.ACCT_NO,

         ESXACCTR.LEVEL_1,

         ESXACCTR.LEVEL_2,

         ESXACCTR.LEVEL_3,

         ESXACCTR.LEVEL_4,

         ESXACCTR.LEVEL_5,

         ESXACCTR.LEVEL_6,

         ESXACCTR.LEVEL_7,

         ESXACCTR.LEVEL_8,

         ESXACCTR.LEVEL_9,

         ESXACCTR.LEVEL_10,

         ESXACCTR.ACCT_TYPE,

         ESXACCTR.ACCT_TITLE AS Object_Name,

    SUBSTRING(LEVEL_3, 1, 1) AS Ledger,

    (SELECT TOP 1 TITLE_1

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_1,



    (SELECT TOP 1 TITLE_2

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_2,



    (SELECT TOP 1 TITLE_3

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_3,



    (SELECT TOP 1 TITLE_4

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_4


FROM [Basie].[GoldStandard].dbo.ESGBUDGD

JOIN [Basie].[GoldStandard].dbo.ESXACCTR

    ON (ESXACCTR.ACCT_ID = ESGBUDGD.ACCT_ID)

    AND (ESXACCTR.ACCT_YEAR = ESGBUDGD.BUD_YEAR)

WHERE BUD_YEAR >= 2013 and (BUD_AMOUNT <> 0)

AND (ESXACCTR.ACCT_TYPE = 'R'

OR ESXACCTR.ACCT_TYPE = 'E')

UNION

SELECT

    ESGBUDGD.ACCT_ID,

    ESXACCTR.ACCT_YEAR AS "Fiscal Year",

	ESGBUDGD.Period,

    ESGBUDGD.BUD_ADJ AS TotalBudgetAmount,

	'Adjustment' AS BudgetType,

    ESXACCTR.ACCT_NO,

         ESXACCTR.LEVEL_1,

         ESXACCTR.LEVEL_2,

         ESXACCTR.LEVEL_3,

         ESXACCTR.LEVEL_4,

         ESXACCTR.LEVEL_5,

         ESXACCTR.LEVEL_6,

         ESXACCTR.LEVEL_7,

         ESXACCTR.LEVEL_8,

         ESXACCTR.LEVEL_9,

         ESXACCTR.LEVEL_10,

         ESXACCTR.ACCT_TYPE,

         ESXACCTR.ACCT_TITLE AS Object_Name,

    SUBSTRING(LEVEL_3, 1, 1) AS Ledger,

    (SELECT TOP 1 TITLE_1

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_1,

    (SELECT TOP 1 TITLE_2

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_2,

    (SELECT TOP 1 TITLE_3

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_3,

    (SELECT TOP 1 TITLE_4

FROM [Basie].[GoldStandard].dbo.esgacttr esga

WHERE ESGBUDGD.BUD_YEAR = esga.ACCT_YEAR and ESGBUDGD.ACCT_ID = esga.ACCT_ID and TITLE_ID = -1) as Title_4

FROM [Basie].[GoldStandard].dbo.ESGBUDGD

JOIN [Basie].[GoldStandard].dbo.ESXACCTR

    ON (ESXACCTR.ACCT_ID = ESGBUDGD.ACCT_ID)

    AND (ESXACCTR.ACCT_YEAR = ESGBUDGD.BUD_YEAR)

WHERE BUD_YEAR >= 2013 and (BUD_ADJ <> 0)

AND (ESXACCTR.ACCT_TYPE = 'R'

OR ESXACCTR.ACCT_TYPE = 'E')
