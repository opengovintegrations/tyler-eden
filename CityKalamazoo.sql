ALTER view vw_opengov_eden_transactions_V4

as





SELECT

    Transaction_Amount = CASE

        WHEN aacct_type = 'E' or aACCT_KIND = 'A' THEN (gdebit_amount - gcredit_amount)

        WHEN aacct_type = 'R' or aACCT_KIND = 'L' THEN (gcredit_amount - gdebit_amount)

        ELSE gcredit_amount - gdebit_amount

    END,

    --gdoc_year                 AS FY,
    -- CUSTOM ABOVE
    esgtranv.aACCT_ID,
    esgtranv.aACCT_KIND,
    esgtranv.aACCT_NO, --Acct_
    esgtranv.aACCT_TITLE, --ObjectName
    esgtranv.aACCT_TYPE, --Acct_Type
    esgtranv.aCASH_INV_CODE,
    esgtranv.aCONTROL_TYPE,
    REPLACE(esgtranv.aLEVEL_1, ' ','') AS aLEVEL_1,
    --esgtranv.aLEVEL_10,
    REPLACE(esgtranv.aLEVEL_2, ' ','') AS aLEVEL_2,
    REPLACE(esgtranv.aLEVEL_3, ' ','') AS aLEVEL_3,
    REPLACE(esgtranv.aLEVEL_4, ' ','') AS aLEVEL_4,
    REPLACE(esgtranv.aLEVEL_5, ' ','') AS aLEVEL_5,
    --esgtranv.aLEVEL_6,
    --esgtranv.aLEVEL_7,
    --esgtranv.aLEVEL_8,
    --esgtranv.aLEVEL_9,
    esgtranv.aMGMT_INFO_CODE,
    esgtranv.aNOMINAL_FLAG,
    esgtranv.apCHK_DATE, --Check_Date
    esgtranv.apCHK_NO, --Check_
    esgtranv.apINV_DATE, --Invoice_Date
    esgtranv.apPO_NUM, --PO_
    esgtranv.apPO_TYPE,
    esgtranv.apVEND_CODE,
    esgtranv.apVEND_FNAME, --Vendor_First_Name
    esgtranv.apVEND_LNAME, --Vendor_Last_Name
    --esgtranv.arCUST_FNAME, --Customer_First_Name
    --esgtranv.arCUST_LNAME, --Customer_Last_Name
    --esgtranv.arCUST_NO,
    --esgtranv.arDOC_NO,
    esgtranv.aREPORT_GROUP1,
    esgtranv.aREPORT_GROUP2,
    esgtranv.aSTATUS_CODE,
    esgtranv.gACCT_PERIOD, --Acct_Period
    esgtranv.gAMOUNT, --GL_Amomunt
    esgtranv.gBUDADJ_AMOUNT, --GL_Budget_Adj_Amount
    esgtranv.gCREDIT_AMOUNT, --Credit_Amount
    esgtranv.gDEBIT_AMOUNT, --Debit_Amount
    esgtranv.gDEBIT_CREDIT, --Debit_or_Credit
    esgtranv.gDESCRIPTION, --Description
    esgtranv.gDOC_SRC,
    esgtranv.gDOC_YEAR,
    esgtranv.gENC_AMOUNT, --GL_Encumbrance_Amount
    --esgtranv.gLINE_NO, --Line_
    esgtranv.gORIG_JOURNAL, --Original_Journal
    esgtranv.gPA_ADJ_AMOUNT, --PA_Amount
    --esgtranv.gSUB_LINE_NO, --Sub_line_
    esgtranv.gTRAN_DOC_NO, --Transaction_Document_
    esgtranv.gTRAN_TYPE, --Transaction_Type
    esgtranv.pALLOC_AMOUNT,
    esgtranv.pAMOUNT,
    esgtranv.pBUDADJ_AMOUNT,
    esgtranv.pCHARGE_TYPE,
    esgtranv.pDEV_DEP_CHARGE,
    esgtranv.pENC_AMOUNT,
    esgtranv.pHOURS,
    esgtranv.pLAB_OR_BEN,
    esgtranv.pPA_CLASS,
    esgtranv.pREC_TYPE,
    esgtranv.pTRAN_AMOUNT,
    esgtranv.sPA_STRING_NO, --String_
    esgtranv.sPROJ_TITLE,
    --esgtranv.sPROJECT_ID,
    esgtranv.sPROJECT_NO,
    esgtranv.sSTR_TITLE,
    esgtranv.sSTR_TITLE_1,
    esgtranv.sSTR_TITLE_2,
    esgtranv.sSTR_TITLE_3,
    esgtranv.sSTRING_LEVEL,
    esgtranv.sSTRING_TYPE,
    esgtranv.sSUB_TASK_NO,
    esgtranv.sTASK_NO,
    --esgtranv.sWORK_ORDER_NO,
    --esgtranv.xAUTO_REV,
    esgtranv.xDOC_DATE, --Document_Date
    esgtranv.xDOC_DESC,
    esgtranv.xDOC_GROUP,
    esgtranv.xDOC_REFERENCE, --Document_Reference
    esgtranv.xPOST_DATE, --Post_Date
    esgtranv.xPOST_NO,
    esgtranv.xSUMMARY_DOC,

    esgacttr.ACCT_ID,
    --esgacttr.ACCT_NO,
    --esgacttr.ACCT_TITLE,
    --esgacttr.ACCT_TYPE,
    --esgacttr.ACCT_YEAR,
    --esgacttr.LEVEL_1,
    --esgacttr.LEVEL_10,
    --esgacttr.LEVEL_11,
    --esgacttr.LEVEL_12,
    --esgacttr.LEVEL_2,
    --esgacttr.LEVEL_3,
    --esgacttr.LEVEL_4,
    --esgacttr.LEVEL_5,
    --esgacttr.LEVEL_6,
    --esgacttr.LEVEL_7,
    --esgacttr.LEVEL_8,
    --esgacttr.LEVEL_9,
    --esgacttr.SUPR_HEADER_1,
    --esgacttr.SUPR_HEADER_10,
    --esgacttr.SUPR_HEADER_11,
    --esgacttr.SUPR_HEADER_12,
    --esgacttr.SUPR_HEADER_2,
    --esgacttr.SUPR_HEADER_3,
    --esgacttr.SUPR_HEADER_4,
    --esgacttr.SUPR_HEADER_5,
    --esgacttr.SUPR_HEADER_6,
    --esgacttr.SUPR_HEADER_7,
    --esgacttr.SUPR_HEADER_8,
    --esgacttr.SUPR_HEADER_9,
    --esgacttr.SUPR_TOTAL_1,
    --esgacttr.SUPR_TOTAL_10,
    --esgacttr.SUPR_TOTAL_11,
    --esgacttr.SUPR_TOTAL_12,
    --esgacttr.SUPR_TOTAL_2,
    --esgacttr.SUPR_TOTAL_3,
    --esgacttr.SUPR_TOTAL_4,
    --esgacttr.SUPR_TOTAL_5,
    --esgacttr.SUPR_TOTAL_6,
    --esgacttr.SUPR_TOTAL_7,
    --esgacttr.SUPR_TOTAL_8,
    --esgacttr.SUPR_TOTAL_9,
    esgacttr.TITLE_1,
    --esgacttr.TITLE_10,
    --esgacttr.TITLE_11,
    --esgacttr.TITLE_12,
    esgacttr.TITLE_2,
    esgacttr.TITLE_3,
    esgacttr.TITLE_4,
    esgacttr.TITLE_5,
    --esgacttr.TITLE_6,
    --esgacttr.TITLE_7,
    --esgacttr.TITLE_8,
    --esgacttr.TITLE_9,
    esgacttr.TITLE_ID,
    'Actuals' AS OGRecordType

FROM

    EDENLIVE.dbo.esgtranv,

    EDENLIVE.dbo.esgacttr

WHERE

    esgtranv.aacct_no = esgacttr.acct_no

    AND esgtranv.gdoc_year = esgacttr.acct_year

    AND esgacttr.title_id = -1

    AND esgtranv.gdoc_year >= 2014

    AND esgacttr.acct_year >= 2014
